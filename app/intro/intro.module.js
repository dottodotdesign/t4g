"use strict";
var platform_1 = require("nativescript-angular/platform");
var core_1 = require("@angular/core");
var intro_routing_1 = require("./intro.routing");
var intro_component_1 = require("./intro.component");
var IntroModule = (function () {
    function IntroModule() {
    }
    return IntroModule;
}());
IntroModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_1.NativeScriptModule,
            intro_routing_1.introRouting
        ],
        declarations: [
            intro_component_1.IntroComponent
        ]
    })
], IntroModule);
exports.IntroModule = IntroModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50cm8ubW9kdWxlLmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiaW50cm8ubW9kdWxlLnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSwwREFBbUU7QUFDbkUsc0NBQXlDO0FBRXpDLGlEQUErQztBQUMvQyxxREFBbUQ7QUFXbkQsSUFBYSxXQUFXO0lBQXhCO0lBQTJCLENBQUM7SUFBRCxrQkFBQztBQUFELENBQUMsQUFBNUIsSUFBNEI7QUFBZixXQUFXO0lBVHZCLGVBQVEsQ0FBQztRQUNSLE9BQU8sRUFBRTtZQUNQLDZCQUFrQjtZQUNsQiw0QkFBWTtTQUNiO1FBQ0QsWUFBWSxFQUFFO1lBQ1osZ0NBQWM7U0FDZjtLQUNGLENBQUM7R0FDVyxXQUFXLENBQUk7QUFBZixrQ0FBVyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9wbGF0Zm9ybVwiO1xuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBpbnRyb1JvdXRpbmcgfSBmcm9tIFwiLi9pbnRyby5yb3V0aW5nXCI7XG5pbXBvcnQgeyBJbnRyb0NvbXBvbmVudCB9IGZyb20gXCIuL2ludHJvLmNvbXBvbmVudFwiO1xuXG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgTmF0aXZlU2NyaXB0TW9kdWxlLFxuICAgIGludHJvUm91dGluZ1xuICBdLFxuICBkZWNsYXJhdGlvbnM6IFtcbiAgICBJbnRyb0NvbXBvbmVudFxuICBdXG59KVxuZXhwb3J0IGNsYXNzIEludHJvTW9kdWxlIHsgfVxuIl19