import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { Page } from "ui/page";
import { topmost } from "ui/frame";
import { isIOS } from "platform";
import * as appSettings from "application-settings";

declare var UIBarStyle: any;

@Component({
    selector: "gr-intro",
    templateUrl: "intro/intro.component.html",
    styleUrls: ["intro/intro.common.css", "intro/intro.component.css"],
})
export class IntroComponent implements OnInit {
    constructor(private routerExtensions: RouterExtensions, private page: Page) { }
    ngOnInit() {
        this.page.actionBarHidden = true;
        if (isIOS) {
            this.page.backgroundSpanUnderStatusBar = true;
        }
    }
    continue() {
        appSettings.setBoolean("hasSeenIntro", true);
        this.routerExtensions.navigate(["/login"], {
            clearHistory: true
        });
    }
}
