import { NativeScriptModule } from "nativescript-angular/platform";
import { NgModule } from "@angular/core";

import { introRouting } from "./intro.routing";
import { IntroComponent } from "./intro.component";

@NgModule({
  imports: [
    NativeScriptModule,
    introRouting
  ],
  declarations: [
    IntroComponent
  ]
})
export class IntroModule { }
