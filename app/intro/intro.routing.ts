import { ModuleWithProviders }  from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { IntroComponent } from "./intro.component";

const introRoutes: Routes = [
  { path: "intro", component: IntroComponent },
];
export const introRouting: ModuleWithProviders = RouterModule.forChild(introRoutes);
