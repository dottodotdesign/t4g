"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var event_service_1 = require("./shared/event.service");
var EventsComponent = (function () {
    function EventsComponent(routerExtensions, eventService) {
        this.routerExtensions = routerExtensions;
        this.eventService = eventService;
    }
    EventsComponent.prototype.ngOnInit = function () {
        this.getEvents();
    };
    EventsComponent.prototype.getEvents = function () {
        var _this = this;
        this.eventService.getEvents().subscribe(function (events) { return _this.events = events; });
    };
    EventsComponent.prototype.showEvent = function (id) {
        this.routerExtensions.navigate(["/event", id]);
    };
    return EventsComponent;
}());
EventsComponent = __decorate([
    core_1.Component({
        selector: "gr-events",
        templateUrl: "events/events.component.html",
        styleUrls: ["events/events.component.css"],
        providers: [event_service_1.EventService]
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions, event_service_1.EventService])
], EventsComponent);
exports.EventsComponent = EventsComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLmNvbXBvbmVudC5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImV2ZW50cy5jb21wb25lbnQudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUFrRDtBQUNsRCxzREFBK0Q7QUFDL0Qsd0RBQW9EO0FBU3BELElBQWEsZUFBZTtJQUd4Qix5QkFBb0IsZ0JBQWtDLEVBQVUsWUFBMEI7UUFBdEUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLGlCQUFZLEdBQVosWUFBWSxDQUFjO0lBQUcsQ0FBQztJQUU5RixrQ0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFNBQVMsRUFBRSxDQUFDO0lBQ3JCLENBQUM7SUFFRCxtQ0FBUyxHQUFUO1FBQUEsaUJBRUM7UUFERyxJQUFJLENBQUMsWUFBWSxDQUFDLFNBQVMsRUFBRSxDQUFDLFNBQVMsQ0FBQyxVQUFBLE1BQU0sSUFBSSxPQUFBLEtBQUksQ0FBQyxNQUFNLEdBQUcsTUFBTSxFQUFwQixDQUFvQixDQUFDLENBQUM7SUFDNUUsQ0FBQztJQUVELG1DQUFTLEdBQVQsVUFBVSxFQUFVO1FBQ2xCLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxRQUFRLENBQUMsQ0FBQyxRQUFRLEVBQUUsRUFBRSxDQUFDLENBQUMsQ0FBQztJQUNqRCxDQUFDO0lBQ0wsc0JBQUM7QUFBRCxDQUFDLEFBaEJELElBZ0JDO0FBaEJZLGVBQWU7SUFOM0IsZ0JBQVMsQ0FBQztRQUNQLFFBQVEsRUFBRSxXQUFXO1FBQ3JCLFdBQVcsRUFBRSw4QkFBOEI7UUFDM0MsU0FBUyxFQUFFLENBQUMsNkJBQTZCLENBQUM7UUFDMUMsU0FBUyxFQUFFLENBQUMsNEJBQVksQ0FBQztLQUM1QixDQUFDO3FDQUl3Qyx5QkFBZ0IsRUFBd0IsNEJBQVk7R0FIakYsZUFBZSxDQWdCM0I7QUFoQlksMENBQWUiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBDb21wb25lbnQsIE9uSW5pdCB9IGZyb20gXCJAYW5ndWxhci9jb3JlXCI7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0IHtFdmVudFNlcnZpY2V9IGZyb20gJy4vc2hhcmVkL2V2ZW50LnNlcnZpY2UnO1xuaW1wb3J0IHtFdmVudH0gZnJvbSAnLi9zaGFyZWQvZXZlbnQnO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogXCJnci1ldmVudHNcIixcbiAgICB0ZW1wbGF0ZVVybDogXCJldmVudHMvZXZlbnRzLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbXCJldmVudHMvZXZlbnRzLmNvbXBvbmVudC5jc3NcIl0sXG4gICAgcHJvdmlkZXJzOiBbRXZlbnRTZXJ2aWNlXVxufSlcbmV4cG9ydCBjbGFzcyBFdmVudHNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICAgIGVycm9yTWVzc2FnZTogc3RyaW5nO1xuICAgIGV2ZW50czogRXZlbnRbXTtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMsIHByaXZhdGUgZXZlbnRTZXJ2aWNlOiBFdmVudFNlcnZpY2UpIHt9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5nZXRFdmVudHMoKTtcbiAgICB9XG5cbiAgICBnZXRFdmVudHMoKSB7XG4gICAgICAgIHRoaXMuZXZlbnRTZXJ2aWNlLmdldEV2ZW50cygpLnN1YnNjcmliZShldmVudHMgPT4gdGhpcy5ldmVudHMgPSBldmVudHMpO1xuICAgIH1cblxuICAgIHNob3dFdmVudChpZDogbnVtYmVyKSB7XG4gICAgICB0aGlzLnJvdXRlckV4dGVuc2lvbnMubmF2aWdhdGUoW1wiL2V2ZW50XCIsIGlkXSk7XG4gICAgfVxufVxuIl19