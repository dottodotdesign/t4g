import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import { ActivatedRoute } from "@angular/router";
import {EventService} from './shared/event.service';
import {Event} from './shared/event';

@Component({
    selector: "gr-events-detail",
    templateUrl: "events/events-detail.component.html",
    styleUrls: [],
    providers: [EventService]
})
export class EventsDetailComponent implements OnInit {

    event: Event;
    constructor(private eventService: EventService, private routerExtensions: RouterExtensions, private route: ActivatedRoute) { }

    ngOnInit() {
        this.getEvent();
    }

    getEvent() {
        const id = +this.route.snapshot.params["id"];
        this.eventService.getEvent(id).subscribe(event => this.event = event);
    }

    public goBack() {
        this.routerExtensions.back();
    }

}
