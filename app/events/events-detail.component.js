"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var router_2 = require("@angular/router");
var event_service_1 = require("./shared/event.service");
var EventsDetailComponent = (function () {
    function EventsDetailComponent(eventService, routerExtensions, route) {
        this.eventService = eventService;
        this.routerExtensions = routerExtensions;
        this.route = route;
    }
    EventsDetailComponent.prototype.ngOnInit = function () {
        this.getEvent();
    };
    EventsDetailComponent.prototype.getEvent = function () {
        var _this = this;
        var id = +this.route.snapshot.params["id"];
        this.eventService.getEvent(id).subscribe(function (event) { return _this.event = event; });
    };
    EventsDetailComponent.prototype.goBack = function () {
        this.routerExtensions.back();
    };
    return EventsDetailComponent;
}());
EventsDetailComponent = __decorate([
    core_1.Component({
        selector: "gr-events-detail",
        templateUrl: "events/events-detail.component.html",
        styleUrls: [],
        providers: [event_service_1.EventService]
    }),
    __metadata("design:paramtypes", [event_service_1.EventService, router_1.RouterExtensions, router_2.ActivatedRoute])
], EventsDetailComponent);
exports.EventsDetailComponent = EventsDetailComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLWRldGFpbC5jb21wb25lbnQuanMiLCJzb3VyY2VSb290IjoiIiwic291cmNlcyI6WyJldmVudHMtZGV0YWlsLmNvbXBvbmVudC50cyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiO0FBQUEsc0NBQWtEO0FBQ2xELHNEQUErRDtBQUMvRCwwQ0FBaUQ7QUFDakQsd0RBQW9EO0FBU3BELElBQWEscUJBQXFCO0lBRzlCLCtCQUFvQixZQUEwQixFQUFVLGdCQUFrQyxFQUFVLEtBQXFCO1FBQXJHLGlCQUFZLEdBQVosWUFBWSxDQUFjO1FBQVUscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtRQUFVLFVBQUssR0FBTCxLQUFLLENBQWdCO0lBQUksQ0FBQztJQUU5SCx3Q0FBUSxHQUFSO1FBQ0ksSUFBSSxDQUFDLFFBQVEsRUFBRSxDQUFDO0lBQ3BCLENBQUM7SUFFRCx3Q0FBUSxHQUFSO1FBQUEsaUJBR0M7UUFGRyxJQUFNLEVBQUUsR0FBRyxDQUFDLElBQUksQ0FBQyxLQUFLLENBQUMsUUFBUSxDQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUMsQ0FBQztRQUM3QyxJQUFJLENBQUMsWUFBWSxDQUFDLFFBQVEsQ0FBQyxFQUFFLENBQUMsQ0FBQyxTQUFTLENBQUMsVUFBQSxLQUFLLElBQUksT0FBQSxLQUFJLENBQUMsS0FBSyxHQUFHLEtBQUssRUFBbEIsQ0FBa0IsQ0FBQyxDQUFDO0lBQzFFLENBQUM7SUFFTSxzQ0FBTSxHQUFiO1FBQ0ksSUFBSSxDQUFDLGdCQUFnQixDQUFDLElBQUksRUFBRSxDQUFDO0lBQ2pDLENBQUM7SUFFTCw0QkFBQztBQUFELENBQUMsQUFsQkQsSUFrQkM7QUFsQlkscUJBQXFCO0lBTmpDLGdCQUFTLENBQUM7UUFDUCxRQUFRLEVBQUUsa0JBQWtCO1FBQzVCLFdBQVcsRUFBRSxxQ0FBcUM7UUFDbEQsU0FBUyxFQUFFLEVBQUU7UUFDYixTQUFTLEVBQUUsQ0FBQyw0QkFBWSxDQUFDO0tBQzVCLENBQUM7cUNBSW9DLDRCQUFZLEVBQTRCLHlCQUFnQixFQUFpQix1QkFBYztHQUhoSCxxQkFBcUIsQ0FrQmpDO0FBbEJZLHNEQUFxQiIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IENvbXBvbmVudCwgT25Jbml0IH0gZnJvbSBcIkBhbmd1bGFyL2NvcmVcIjtcbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tIFwibmF0aXZlc2NyaXB0LWFuZ3VsYXIvcm91dGVyXCI7XG5pbXBvcnQgeyBBY3RpdmF0ZWRSb3V0ZSB9IGZyb20gXCJAYW5ndWxhci9yb3V0ZXJcIjtcbmltcG9ydCB7RXZlbnRTZXJ2aWNlfSBmcm9tICcuL3NoYXJlZC9ldmVudC5zZXJ2aWNlJztcbmltcG9ydCB7RXZlbnR9IGZyb20gJy4vc2hhcmVkL2V2ZW50JztcblxuQENvbXBvbmVudCh7XG4gICAgc2VsZWN0b3I6IFwiZ3ItZXZlbnRzLWRldGFpbFwiLFxuICAgIHRlbXBsYXRlVXJsOiBcImV2ZW50cy9ldmVudHMtZGV0YWlsLmNvbXBvbmVudC5odG1sXCIsXG4gICAgc3R5bGVVcmxzOiBbXSxcbiAgICBwcm92aWRlcnM6IFtFdmVudFNlcnZpY2VdXG59KVxuZXhwb3J0IGNsYXNzIEV2ZW50c0RldGFpbENvbXBvbmVudCBpbXBsZW1lbnRzIE9uSW5pdCB7XG5cbiAgICBldmVudDogRXZlbnQ7XG4gICAgY29uc3RydWN0b3IocHJpdmF0ZSBldmVudFNlcnZpY2U6IEV2ZW50U2VydmljZSwgcHJpdmF0ZSByb3V0ZXJFeHRlbnNpb25zOiBSb3V0ZXJFeHRlbnNpb25zLCBwcml2YXRlIHJvdXRlOiBBY3RpdmF0ZWRSb3V0ZSkgeyB9XG5cbiAgICBuZ09uSW5pdCgpIHtcbiAgICAgICAgdGhpcy5nZXRFdmVudCgpO1xuICAgIH1cblxuICAgIGdldEV2ZW50KCkge1xuICAgICAgICBjb25zdCBpZCA9ICt0aGlzLnJvdXRlLnNuYXBzaG90LnBhcmFtc1tcImlkXCJdO1xuICAgICAgICB0aGlzLmV2ZW50U2VydmljZS5nZXRFdmVudChpZCkuc3Vic2NyaWJlKGV2ZW50ID0+IHRoaXMuZXZlbnQgPSBldmVudCk7XG4gICAgfVxuXG4gICAgcHVibGljIGdvQmFjaygpIHtcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLmJhY2soKTtcbiAgICB9XG5cbn1cbiJdfQ==