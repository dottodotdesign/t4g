import { NativeScriptModule } from "nativescript-angular/platform";
import { NgModule } from "@angular/core";

import { eventsRouting } from "./events.routing";
import { EventsComponent } from "./events.component";
import { EventsDetailComponent } from "./events-detail.component";
import { SharedModule } from '../shared/shared.module';
@NgModule({
  imports: [
    NativeScriptModule,
    eventsRouting,
    SharedModule
  ],
  declarations: [
    EventsComponent,
    EventsDetailComponent
  ]
})
export class EventsModule { }
