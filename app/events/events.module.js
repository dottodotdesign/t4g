"use strict";
var platform_1 = require("nativescript-angular/platform");
var core_1 = require("@angular/core");
var events_routing_1 = require("./events.routing");
var events_component_1 = require("./events.component");
var events_detail_component_1 = require("./events-detail.component");
var shared_module_1 = require("../shared/shared.module");
var EventsModule = (function () {
    function EventsModule() {
    }
    return EventsModule;
}());
EventsModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_1.NativeScriptModule,
            events_routing_1.eventsRouting,
            shared_module_1.SharedModule
        ],
        declarations: [
            events_component_1.EventsComponent,
            events_detail_component_1.EventsDetailComponent
        ]
    })
], EventsModule);
exports.EventsModule = EventsModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiZXZlbnRzLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImV2ZW50cy5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLDBEQUFtRTtBQUNuRSxzQ0FBeUM7QUFFekMsbURBQWlEO0FBQ2pELHVEQUFxRDtBQUNyRCxxRUFBa0U7QUFDbEUseURBQXVEO0FBWXZELElBQWEsWUFBWTtJQUF6QjtJQUE0QixDQUFDO0lBQUQsbUJBQUM7QUFBRCxDQUFDLEFBQTdCLElBQTZCO0FBQWhCLFlBQVk7SUFYeEIsZUFBUSxDQUFDO1FBQ1IsT0FBTyxFQUFFO1lBQ1AsNkJBQWtCO1lBQ2xCLDhCQUFhO1lBQ2IsNEJBQVk7U0FDYjtRQUNELFlBQVksRUFBRTtZQUNaLGtDQUFlO1lBQ2YsK0NBQXFCO1NBQ3RCO0tBQ0YsQ0FBQztHQUNXLFlBQVksQ0FBSTtBQUFoQixvQ0FBWSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IE5hdGl2ZVNjcmlwdE1vZHVsZSB9IGZyb20gXCJuYXRpdmVzY3JpcHQtYW5ndWxhci9wbGF0Zm9ybVwiO1xuaW1wb3J0IHsgTmdNb2R1bGUgfSBmcm9tIFwiQGFuZ3VsYXIvY29yZVwiO1xuXG5pbXBvcnQgeyBldmVudHNSb3V0aW5nIH0gZnJvbSBcIi4vZXZlbnRzLnJvdXRpbmdcIjtcbmltcG9ydCB7IEV2ZW50c0NvbXBvbmVudCB9IGZyb20gXCIuL2V2ZW50cy5jb21wb25lbnRcIjtcbmltcG9ydCB7IEV2ZW50c0RldGFpbENvbXBvbmVudCB9IGZyb20gXCIuL2V2ZW50cy1kZXRhaWwuY29tcG9uZW50XCI7XG5pbXBvcnQgeyBTaGFyZWRNb2R1bGUgfSBmcm9tICcuLi9zaGFyZWQvc2hhcmVkLm1vZHVsZSc7XG5ATmdNb2R1bGUoe1xuICBpbXBvcnRzOiBbXG4gICAgTmF0aXZlU2NyaXB0TW9kdWxlLFxuICAgIGV2ZW50c1JvdXRpbmcsXG4gICAgU2hhcmVkTW9kdWxlXG4gIF0sXG4gIGRlY2xhcmF0aW9uczogW1xuICAgIEV2ZW50c0NvbXBvbmVudCxcbiAgICBFdmVudHNEZXRhaWxDb21wb25lbnRcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBFdmVudHNNb2R1bGUgeyB9XG4iXX0=