import { Component, OnInit } from "@angular/core";
import { RouterExtensions } from "nativescript-angular/router";
import {EventService} from './shared/event.service';
import {Event} from './shared/event';

@Component({
    selector: "gr-events",
    templateUrl: "events/events.component.html",
    styleUrls: ["events/events.component.css"],
    providers: [EventService]
})
export class EventsComponent implements OnInit {
    errorMessage: string;
    events: Event[];
    constructor(private routerExtensions: RouterExtensions, private eventService: EventService) {}

    ngOnInit() {
        this.getEvents();
    }

    getEvents() {
        this.eventService.getEvents().subscribe(events => this.events = events);
    }

    showEvent(id: number) {
      this.routerExtensions.navigate(["/event", id]);
    }
}
