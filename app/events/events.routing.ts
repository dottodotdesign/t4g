import { ModuleWithProviders }  from "@angular/core";
import { Routes, RouterModule } from "@angular/router";

import { EventsComponent } from "./events.component";
import { EventsDetailComponent } from "./events-detail.component";

const eventsRoutes: Routes = [
  { path: "events", component: EventsComponent },
  { path: "event/:id", component: EventsDetailComponent },
];
export const eventsRouting: ModuleWithProviders = RouterModule.forChild(eventsRoutes);
