export class Event {
    id: number;
    title: string;
    start_date: string;
    end_date: string;
    image: string;
}
