import { Injectable } from "@angular/core";
import { Http, Response } from "@angular/http";
import { Observable, BehaviorSubject } from "rxjs/Rx";
import "rxjs/add/operator/map";


import { BackendService } from "../../shared/services/backend.service";
import { Event } from "./event";

@Injectable()
export class EventService {
    constructor(private http: Http) { }

    getEvents() {
        return this.http.get(BackendService.apiUrl + "events").map(this.extractData).catch(this.handleError);
    }

    getEvent(id: number) {
      return this.http.get(BackendService.apiUrl + `events/${id}`).map(res => res.json()).catch(this.handleError);
    }

    private extractData(res: Response) {
        let body = res.json();
        return body.events || {};
    }

    private handleError(error: Response | any) {
        let errMsg: string;
        if (error instanceof Response) {
            const body = error.json() || '';
            const err = body.error || JSON.stringify(body);
            errMsg = `${error.status} - ${error.statusText || ''} ${err}`;
        } else {
            errMsg = error.message ? error.message : error.toString();
        }
        console.error(errMsg);
        return Observable.throw(errMsg);
    }
}
