import {
    Component
} from '@angular/core';

import { RouterExtensions } from 'nativescript-angular/router';

@Component({
    selector: 'back-button',
    templateUrl: 'shared/components/back-button/back-button.component.html'
})
export class BackButtonComponent {
    constructor(private routerExtensions: RouterExtensions) { }
    public goBack() {
        this.routerExtensions.back();
    }

}
