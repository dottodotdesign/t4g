"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var BackButtonComponent = (function () {
    function BackButtonComponent(routerExtensions) {
        this.routerExtensions = routerExtensions;
    }
    BackButtonComponent.prototype.goBack = function () {
        this.routerExtensions.back();
    };
    return BackButtonComponent;
}());
BackButtonComponent = __decorate([
    core_1.Component({
        selector: 'back-button',
        templateUrl: 'shared/components/back-button/back-button.component.html'
    }),
    __metadata("design:paramtypes", [router_1.RouterExtensions])
], BackButtonComponent);
exports.BackButtonComponent = BackButtonComponent;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiYmFjay1idXR0b24uY29tcG9uZW50LmpzIiwic291cmNlUm9vdCI6IiIsInNvdXJjZXMiOlsiYmFjay1idXR0b24uY29tcG9uZW50LnRzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiI7QUFBQSxzQ0FFdUI7QUFFdkIsc0RBQStEO0FBTS9ELElBQWEsbUJBQW1CO0lBQzVCLDZCQUFvQixnQkFBa0M7UUFBbEMscUJBQWdCLEdBQWhCLGdCQUFnQixDQUFrQjtJQUFJLENBQUM7SUFDcEQsb0NBQU0sR0FBYjtRQUNJLElBQUksQ0FBQyxnQkFBZ0IsQ0FBQyxJQUFJLEVBQUUsQ0FBQztJQUNqQyxDQUFDO0lBRUwsMEJBQUM7QUFBRCxDQUFDLEFBTkQsSUFNQztBQU5ZLG1CQUFtQjtJQUovQixnQkFBUyxDQUFDO1FBQ1AsUUFBUSxFQUFFLGFBQWE7UUFDdkIsV0FBVyxFQUFFLDBEQUEwRDtLQUMxRSxDQUFDO3FDQUV3Qyx5QkFBZ0I7R0FEN0MsbUJBQW1CLENBTS9CO0FBTlksa0RBQW1CIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IHtcbiAgICBDb21wb25lbnRcbn0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5cbmltcG9ydCB7IFJvdXRlckV4dGVuc2lvbnMgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9yb3V0ZXInO1xuXG5AQ29tcG9uZW50KHtcbiAgICBzZWxlY3RvcjogJ2JhY2stYnV0dG9uJyxcbiAgICB0ZW1wbGF0ZVVybDogJ3NoYXJlZC9jb21wb25lbnRzL2JhY2stYnV0dG9uL2JhY2stYnV0dG9uLmNvbXBvbmVudC5odG1sJ1xufSlcbmV4cG9ydCBjbGFzcyBCYWNrQnV0dG9uQ29tcG9uZW50IHtcbiAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHsgfVxuICAgIHB1YmxpYyBnb0JhY2soKSB7XG4gICAgICAgIHRoaXMucm91dGVyRXh0ZW5zaW9ucy5iYWNrKCk7XG4gICAgfVxuXG59XG4iXX0=