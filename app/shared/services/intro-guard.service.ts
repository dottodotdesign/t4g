import { Injectable }     from '@angular/core';
import { CanActivate }    from '@angular/router';
import { RouterExtensions } from "nativescript-angular/router";
import * as appSettings from "application-settings";

@Injectable()
export class IntroGuard implements CanActivate {
     constructor(private routerExtensions: RouterExtensions) {}
    canActivate(): boolean  {
        return this.checkIntro();
    }
    checkIntro(): boolean {
        if (appSettings.getBoolean("hasSeenIntro")) return true;
        this.routerExtensions.navigate(["/intro"], { clearHistory: true });
        return false;
    }
}
