"use strict";
var core_1 = require("@angular/core");
var router_1 = require("nativescript-angular/router");
var appSettings = require("application-settings");
var IntroGuard = (function () {
    function IntroGuard(routerExtensions) {
        this.routerExtensions = routerExtensions;
    }
    IntroGuard.prototype.canActivate = function () {
        return this.checkIntro();
    };
    IntroGuard.prototype.checkIntro = function () {
        if (appSettings.getBoolean("hasSeenIntro"))
            return true;
        this.routerExtensions.navigate(["/intro"], { clearHistory: true });
        return false;
    };
    return IntroGuard;
}());
IntroGuard = __decorate([
    core_1.Injectable(),
    __metadata("design:paramtypes", [router_1.RouterExtensions])
], IntroGuard);
exports.IntroGuard = IntroGuard;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiaW50cm8tZ3VhcmQuc2VydmljZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbImludHJvLWd1YXJkLnNlcnZpY2UudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUErQztBQUUvQyxzREFBK0Q7QUFDL0Qsa0RBQW9EO0FBR3BELElBQWEsVUFBVTtJQUNsQixvQkFBb0IsZ0JBQWtDO1FBQWxDLHFCQUFnQixHQUFoQixnQkFBZ0IsQ0FBa0I7SUFBRyxDQUFDO0lBQzNELGdDQUFXLEdBQVg7UUFDSSxNQUFNLENBQUMsSUFBSSxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQzdCLENBQUM7SUFDRCwrQkFBVSxHQUFWO1FBQ0ksRUFBRSxDQUFDLENBQUMsV0FBVyxDQUFDLFVBQVUsQ0FBQyxjQUFjLENBQUMsQ0FBQztZQUFDLE1BQU0sQ0FBQyxJQUFJLENBQUM7UUFDeEQsSUFBSSxDQUFDLGdCQUFnQixDQUFDLFFBQVEsQ0FBQyxDQUFDLFFBQVEsQ0FBQyxFQUFFLEVBQUUsWUFBWSxFQUFFLElBQUksRUFBRSxDQUFDLENBQUM7UUFDbkUsTUFBTSxDQUFDLEtBQUssQ0FBQztJQUNqQixDQUFDO0lBQ0wsaUJBQUM7QUFBRCxDQUFDLEFBVkQsSUFVQztBQVZZLFVBQVU7SUFEdEIsaUJBQVUsRUFBRTtxQ0FFOEIseUJBQWdCO0dBRDlDLFVBQVUsQ0FVdEI7QUFWWSxnQ0FBVSIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCB7IEluamVjdGFibGUgfSAgICAgZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBDYW5BY3RpdmF0ZSB9ICAgIGZyb20gJ0Bhbmd1bGFyL3JvdXRlcic7XG5pbXBvcnQgeyBSb3V0ZXJFeHRlbnNpb25zIH0gZnJvbSBcIm5hdGl2ZXNjcmlwdC1hbmd1bGFyL3JvdXRlclwiO1xuaW1wb3J0ICogYXMgYXBwU2V0dGluZ3MgZnJvbSBcImFwcGxpY2F0aW9uLXNldHRpbmdzXCI7XG5cbkBJbmplY3RhYmxlKClcbmV4cG9ydCBjbGFzcyBJbnRyb0d1YXJkIGltcGxlbWVudHMgQ2FuQWN0aXZhdGUge1xuICAgICBjb25zdHJ1Y3Rvcihwcml2YXRlIHJvdXRlckV4dGVuc2lvbnM6IFJvdXRlckV4dGVuc2lvbnMpIHt9XG4gICAgY2FuQWN0aXZhdGUoKTogYm9vbGVhbiAge1xuICAgICAgICByZXR1cm4gdGhpcy5jaGVja0ludHJvKCk7XG4gICAgfVxuICAgIGNoZWNrSW50cm8oKTogYm9vbGVhbiB7XG4gICAgICAgIGlmIChhcHBTZXR0aW5ncy5nZXRCb29sZWFuKFwiaGFzU2VlbkludHJvXCIpKSByZXR1cm4gdHJ1ZTtcbiAgICAgICAgdGhpcy5yb3V0ZXJFeHRlbnNpb25zLm5hdmlnYXRlKFtcIi9pbnRyb1wiXSwgeyBjbGVhckhpc3Rvcnk6IHRydWUgfSk7XG4gICAgICAgIHJldHVybiBmYWxzZTtcbiAgICB9XG59XG4iXX0=