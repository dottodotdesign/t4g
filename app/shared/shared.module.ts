import { NgModule } from '@angular/core';

import { NativeScriptModule } from 'nativescript-angular/platform';
import { SIDEDRAWER_DIRECTIVES } from 'nativescript-telerik-ui/sidedrawer/angular';

import { SideDrawerPageComponent } from './components/side-drawer-page';

@NgModule({
  imports: [
    NativeScriptModule,
  ],
  declarations: [
    SIDEDRAWER_DIRECTIVES,
    SideDrawerPageComponent
  ],
  exports: [
    SideDrawerPageComponent,
  ]
})
export class SharedModule {

}
