"use strict";
var core_1 = require("@angular/core");
var platform_1 = require("nativescript-angular/platform");
var angular_1 = require("nativescript-telerik-ui/sidedrawer/angular");
var side_drawer_page_1 = require("./components/side-drawer-page");
var SharedModule = (function () {
    function SharedModule() {
    }
    return SharedModule;
}());
SharedModule = __decorate([
    core_1.NgModule({
        imports: [
            platform_1.NativeScriptModule,
        ],
        declarations: [
            angular_1.SIDEDRAWER_DIRECTIVES,
            side_drawer_page_1.SideDrawerPageComponent
        ],
        exports: [
            side_drawer_page_1.SideDrawerPageComponent,
        ]
    })
], SharedModule);
exports.SharedModule = SharedModule;
//# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoic2hhcmVkLm1vZHVsZS5qcyIsInNvdXJjZVJvb3QiOiIiLCJzb3VyY2VzIjpbInNoYXJlZC5tb2R1bGUudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtBQUFBLHNDQUF5QztBQUV6QywwREFBbUU7QUFDbkUsc0VBQW1GO0FBRW5GLGtFQUF3RTtBQWN4RSxJQUFhLFlBQVk7SUFBekI7SUFFQSxDQUFDO0lBQUQsbUJBQUM7QUFBRCxDQUFDLEFBRkQsSUFFQztBQUZZLFlBQVk7SUFaeEIsZUFBUSxDQUFDO1FBQ1IsT0FBTyxFQUFFO1lBQ1AsNkJBQWtCO1NBQ25CO1FBQ0QsWUFBWSxFQUFFO1lBQ1osK0JBQXFCO1lBQ3JCLDBDQUF1QjtTQUN4QjtRQUNELE9BQU8sRUFBRTtZQUNQLDBDQUF1QjtTQUN4QjtLQUNGLENBQUM7R0FDVyxZQUFZLENBRXhCO0FBRlksb0NBQVkiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgeyBOZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5pbXBvcnQgeyBOYXRpdmVTY3JpcHRNb2R1bGUgfSBmcm9tICduYXRpdmVzY3JpcHQtYW5ndWxhci9wbGF0Zm9ybSc7XG5pbXBvcnQgeyBTSURFRFJBV0VSX0RJUkVDVElWRVMgfSBmcm9tICduYXRpdmVzY3JpcHQtdGVsZXJpay11aS9zaWRlZHJhd2VyL2FuZ3VsYXInO1xuXG5pbXBvcnQgeyBTaWRlRHJhd2VyUGFnZUNvbXBvbmVudCB9IGZyb20gJy4vY29tcG9uZW50cy9zaWRlLWRyYXdlci1wYWdlJztcblxuQE5nTW9kdWxlKHtcbiAgaW1wb3J0czogW1xuICAgIE5hdGl2ZVNjcmlwdE1vZHVsZSxcbiAgXSxcbiAgZGVjbGFyYXRpb25zOiBbXG4gICAgU0lERURSQVdFUl9ESVJFQ1RJVkVTLFxuICAgIFNpZGVEcmF3ZXJQYWdlQ29tcG9uZW50XG4gIF0sXG4gIGV4cG9ydHM6IFtcbiAgICBTaWRlRHJhd2VyUGFnZUNvbXBvbmVudCxcbiAgXVxufSlcbmV4cG9ydCBjbGFzcyBTaGFyZWRNb2R1bGUge1xuXG59XG4iXX0=